---
layout: page
title: "diffoscope: in-depth comparison of files, archives, and directories"
---

# {{ site.title }}

<div class="lead">{{ site.description }}</div>

<br>

*diffoscope* tries to get to the bottom of what makes files or directories different. It will recursively unpack archives of many kinds and transform various binary formats into more human-readable form to compare them. It can compare two tarballs, ISO images, or PDF just as easily.

**Latest release**: {{ site.data.diffoscope.latest_release.version }} ({{ site.data.diffoscope.latest_release.date|date: "%d %b %Y"  }})

<br>

You can try it now using Docker:

    echo foo > file1
    echo bar > file2
    docker run --rm -t -w $(pwd) -v $(pwd):$(pwd):ro \
          registry.salsa.debian.org/reproducible-builds/diffoscope file1 file2

... or try it on [**try.diffoscope.org**](https://try.diffoscope.org/)!

## News

{% for x in site.posts limit: 5 %}
* [{{ x.title }}]({{ x.url }}) ({{ x.date|date: "%d %b %Y" }}){% endfor %}

([RSS]({{ "feed.xml" | relative_url }}))

## Examples

[![Example of diffoscope HTML output]({{ "examples/https-everywhere-5.0.6_vs_5.0.7.thumbnail.png" | relative_url }}?{{ site.time | date: '%s%N' }})]({{ "examples/https-everywhere-5.0.6_vs_5.0.7.html"  | relative_url }})

[Firefox extensions compared (HTML output)]({{ "examples/https-everywhere-5.0.6_vs_5.0.7.html" | relative_url }})

[![Example of diffoscope text output]({{ "examples/igerman98_20131206-5.thumbnail.png" | relative_url }}?{{ site.time | date: '%s%N' }})]({{ "examples/igerman98_20131206-5.txt" | relative_url }})

[Debian packages compared (text output)]({{ "/examples/igerman98_20131206-5.txt" | relative_url }})

&hellip; and more [examples on tests.reproducible-builds.org](https://tests.reproducible-builds.org/debian/unstable/amd64/index_FTBR.html). You can also view an [demonstration interactive shell session]({{ "static/images/progressbar.gif" | relative_url }}?{{ site.time | date: '%s%N' }}) (220K).

## Features

* Command-line interface

* Text and HTML ouput

* {{ site.data.diffoscope.description }}

* Fallback on hexdump comparison

* Fuzzy-matching to handle renamings

* &hellip; and many more!

## Get diffoscope

* **<a href="https://try.diffoscope.org/">Try it online!</a>**

* Via <a href="https://pypi.python.org/">`pip`</a>:<br /> `pip install diffoscope`<br /> *Note:* You might still want to install Python modules from <a href="https://github.com/trendmicro/tlsh">tlsh</a> and <a href="http://rpm.org/">rpm</a> and other external tools to get more meaningful results. Use `diffoscope --list-tools` to get the full list.

* On <a href="https://www.debian.org/">Debian</a> and derivatives:<br />
`apt install diffoscope`

* Via Docker:<br>
  `docker run --rm -t -w $(pwd) -v $(pwd):$(pwd):ro registry.salsa.debian.org/reproducible-builds/diffoscope`

* On <a href="https://fedoraproject.org">Fedora</a> based systems:<br/>
`dnf install diffoscope`

* On <a href="https://opensuse.org">openSUSE</a>:<br/>
`zypper in diffoscope`

* On <a href="https://archlinux.org">Arch Linux</a>:<br/>
`pacman -S diffoscope`

* On <a href="https://freebsd.org">FreeBSD</a> based systems:<br/>
`pkg install py38-diffoscope`

* On <a href="https://openbsd.org">OpenBSD</a> based systems (<a href="https://github.com/openbsd/ports/tree/master/sysutils/diffoscope">packaging</a>):<br/>
`pkg_add diffoscope`

* On <a href="https://nixos.org/">NixOS/nixpkgs</a>:<br/>`nix-shell -p diffoscope`

* On <a href="https://brew.sh/">Homebrew</a>:<br/>`brew install diffoscope`

* <a href="/archive">Source tarballs</a>

* Through Git:<br />
`git clone {{ site.salsa_url }}.git`

## Contribute

*diffoscope* is developed within the <a href="https://reproducible-builds.org/">“Reproducible builds” effort</a>.

* <a href="{{ site.salsa_url }}">Git repository</a>

* <a href="{{ site.salsa_url }}/issues">Issues and feature requests</a>
  * <a href="{{ site.salsa_url }}/issues/new">File a new issue!</a> (<a href="https://reproducible-builds.org/contribute/salsa/">registration instructions</a>)

* <a href="https://lists.reproducible-builds.org/listinfo/diffoscope">Users and developers mailing-list</a>

* `#reproducible-builds` and/or `#debian-reproducible` on <a href="https://oftc.net/">OFTC</a>

Extending *diffoscope* to support new formats is quite straightforward in most cases. It also has a comprehensive test suite. Patches welcome!

## Similar software

* <a href="https://github.com/lvc/pkgdiff">pkgdiff</a>

* `pkg-diff.sh` from <a href="https://github.com/openSUSE/build-compare">Open Build Service build-compare</a>

## License

*diffoscope* is free software licensed under the <a href="https://www.gnu.org/licenses/gpl.html">GNU General Public License version 3</a> or later.

## Contributors

{{ site.data.diffoscope.contributors|array_to_sentence_string:"and" }}.
