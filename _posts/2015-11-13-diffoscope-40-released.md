---
layout: post
title: diffoscope 40 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `40`. This version includes the following changes:

```
* Add missing files in PyPI package.
* Use libarchive instead of debian.arfile to extract .deb.
* Make python-debian optional. This will help packaging diffoscope in other
  distributions.
* Add 'Programming Language' to PyPI classifiers.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
