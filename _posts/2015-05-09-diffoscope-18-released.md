---
layout: post
title: diffoscope 18 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `18`. This version includes the following changes:

```
[ Reiner Herrmann ]
* Use zip comparator for pk3 files.
* Use text comparator for info files.
* Drop mime type for info files.
* Remove limit of diff output again.

[ Jérémy Bobbio ]
* Assume tar member names are UTF-8 encoded. This allows debbindiff to
  process drmips.
* Write a text report on stdout as default behavior.
* Allow both --html and --text to produce output in one run.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
