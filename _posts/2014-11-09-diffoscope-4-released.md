---
layout: post
title: diffoscope 4 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `4`. This version includes the following changes:

```
* Look harder for differences in zip files if needed.
* Avoid deadlock when calling vimdiff. (Closes: #764131, #764420)
* Add support for an external CSS. (Closes: #764470)
* Improve default CSS to keep tables from overflowing.
* Bump Standards-Version.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
