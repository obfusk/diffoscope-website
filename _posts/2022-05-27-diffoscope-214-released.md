---
layout: post
title: diffoscope 214 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `214`. This version includes the following changes:

```
[ Chris Lamb ]
* Support both python-argcomplete 1.x and 2.x.

[ Vagrant Cascadian ]
* Add external tool on GNU Guix for xb-tool.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
