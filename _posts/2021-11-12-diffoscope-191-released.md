---
layout: post
title: diffoscope 191 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `191`. This version includes the following changes:

```
[ Chris Lamb ]
* Detect XML files as XML files if either file(1) claims if they are XML
  files, or if they are named .xml.
  (Closes: #999438, reproducible-builds/diffoscope#287)
* Don't reject Debian .changes files if they contain non-printable
  characters. (Closes: reproducible-builds/diffoscope#286)
* Continue loading a .changes file even if the referenced files inside it do
  not exist, but include a comment in the diff as a result.
* Log the reason if we cannot load a Debian .changes file.

[ Zbigniew Jędrzejewski-Szmek ]
* Fix inverted logic in the assert_diff_startswith() utility.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
