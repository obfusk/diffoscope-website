---
layout: post
title: diffoscope 128 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `128`. This version includes the following changes:

```
* Query the container for the full path of the parallel R .rdx file for a
  .rdb file as well as looking in the same directory. This ensures that
  comparing two .deb/.changes files shows a varying path introduced in
  version 127.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
