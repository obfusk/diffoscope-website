---
layout: post
title: diffoscope 205 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `205`. This version includes the following changes:

```
* Fix a file(1)-related regression where .changes files that contained
  non-ASCII text were not identified as being .changes files, resulting in
  seemingly arbitrary packages on tests.reproducible-builds.org and elswhere
  not comparing the package at all. The non-ASCII parts could have been in
  the Maintainer or in the upload changelog, so we were effectively
  penalising anyone outside of the Anglosphere.
  (Closes: reproducible-builds/diffoscope#291)
* Don't print a warning to the console regarding NT_GNU_BUILD_ID changes in
  ELF binaries.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
