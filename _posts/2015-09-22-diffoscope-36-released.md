---
layout: post
title: diffoscope 36 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `36`. This version includes the following changes:

```
* Read Debian .changes file as utf-8 encoded.
* Add missing encode() for the 'too much input for diff' message.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
