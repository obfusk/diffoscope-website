---
layout: post
title: diffoscope 196 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `196`. This version includes the following changes:

```
[ Roland Clobus ]
* Add a comment/annotation when the GNU_BUILD_ID field has been modified.

[ Brent Spillner ]
* Fix the "Black" version detection.

[ Chris Lamb ]
* Replace "token" with anonymous variable "x" in order to remove extra lines.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
