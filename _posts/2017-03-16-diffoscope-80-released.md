---
layout: post
title: diffoscope 80 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `80`. This version includes the following changes:

```
* Ensure that we really are using ImageMagick and not, for example, the
  GraphicsMagick compatibility layer installed by
  graphicsmagick-imagemagick-compat. (Closes: #857940)
* Factor out the unicode decoding of the identify -version output.
* travis.yml: Don't build tags.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
