---
layout: post
title: diffoscope 159 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `159`. This version includes the following changes:

```
[ Chris Lamb ]
* Show "ordering differences only" in strings(1) output.
  (Closes: reproducible-builds/diffoscope#216)
* Don't alias output from "os.path.splitext" to variables that we do not end
  up using.
* Don't raise exceptions when cleaning up after a guestfs cleanup failure.

[ Jean-Romain Garnier ]
* Make "Command" subclass a new generic Operation class.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
