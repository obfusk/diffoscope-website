---
layout: post
title: diffoscope 38 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `38`. This version includes the following changes:

```
* Fix dh_python3 package overrides so we get a correct versioned
  Depends on python3-tlsh.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
