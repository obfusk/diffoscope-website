---
layout: post
title: diffoscope 226 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `226`. This version includes the following changes:

```
[ Christopher Baines ]
* Add an lzip comparator with tests.

[ Chris Lamb ]
* Add support for comparing the "text" content of HTML files using html2text.
  (Closes: #1022209, reproducible-builds/diffoscope#318)
* Misc/test improvements:
  * Drop the ALLOWED_TEST_FILES test; it's mostly just annoying.
  * Drop other copyright notices from lzip.py and test_lzip.py.
  * Use assert_diff helper in test_lzip.py.
  * Pylint tests/test_source.py.

[ Mattia Rizzolo ]
* Add lzip to debian dependencies.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
