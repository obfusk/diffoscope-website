---
layout: post
title: diffoscope 186 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `186`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't call close_archive when garbage-collecting Archive instances unless
  open_archive returned successfully. This prevents, amongst others, an
  AttributeError traceback due to PGPContainer's cleanup routines assuming
  that its temporary directory had been created.
  (Closes: reproducible-builds/diffoscope#276)
* Ensure that the string "RPM archives" exists in the package description,
  regardless of whether python3-rpm is installed or not at build time.

[ Jean-Romain Garnier ]
* Fix the LVM Macho comparator for non-x86-64 architectures.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
