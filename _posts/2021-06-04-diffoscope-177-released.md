---
layout: post
title: diffoscope 177 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `177`. This version includes the following changes:

```
[ Keith Smiley ]
* Improve support for Apple "provisioning profiles".
* Fix ignoring objdump tests on MacOS.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
