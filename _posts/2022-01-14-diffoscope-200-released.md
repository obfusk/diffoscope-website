---
layout: post
title: diffoscope 200 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `200`. This version includes the following changes:

```
* Even if a Sphinx .inv inventory file is labelled "The remainder of this
  file is compressed using zlib", it might not actually be. In this case,
  don't traceback, and simply return the original content.
  (Closes: reproducible-builds/diffoscope#299)
* Update "X has been modified after NT_GNU_BUILD_ID has been applied" message
  to, for instance, not duplicating the full filename in the primary
  diffoscope's output.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
