---
layout: post
title: diffoscope 143 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `143`. This version includes the following changes:

```
* Add support for .p7c and .p7b certificates.
  (Closes: reproducible-builds/diffoscope#94)
* Add "pdftotext" as a requirement to run the PDF test_metadata text. Special
  thanks to Chocimier/Piotr for the debugging work.
  (Closes: reproducible-builds/diffoscope#99)
* Improve the documentation of FALLBACK_FILE_TYPE_HEADER_PREFIX and
  FILE_TYPE_HEADER_PREFIX to note that only the first 16 bytes are used.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
