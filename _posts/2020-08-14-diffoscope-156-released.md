---
layout: post
title: diffoscope 156 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `156`. This version includes the following changes:

```
[ Chris Lamb ]
* Update PPU tests for compatibility with Free Pascal versions 3.2.0 or
  greater. (Closes: #968124)
* Emit a debug-level logging message when our ppudump(1) version does not
  match file header.
* Add and use an assert_diff helper that loads and compares a fixture output
  to avoid a bunch of test boilerplate.

[ Frazer Clews ]
* Apply some pylint suggestions to the codebase.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
