---
layout: post
title: diffoscope 131 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `131`. This version includes the following changes:

```
* Clarify in the HTML and text outputs that the limits are per output format,
  not global. (Closes: #944882, reproducible-builds/diffoscope#76)
  - Bump the previous "max_page_size" limit from 400 kB to 4 MB.
* Update an Android manifest test to reflect that parsed XML attributes are
  returned in a sorted or otherwise novel manner under Python 3.8.
* Update code to reflect version 19.10b0 of the black source code
  reformatter and don't run our self-test for versions earlier than this as
  it will generate different results and thus fail.
* Limit .dsc and .buildinfo matching; don't attempt to compare them as
  ELF sections or similar. (Closes: reproducible-builds/diffoscope#77)
* Add a comment that the text_ascii{1,2} test fixture files are used in
  multiple places so are not trivial to generate on the fly.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
