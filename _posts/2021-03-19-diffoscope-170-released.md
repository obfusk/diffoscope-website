---
layout: post
title: diffoscope 170 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `170`. This version includes the following changes:

```
[ Chris Lamb ]
* Avoid frequent long lines in RPM header outputs that cause very very slow
  HTML outputs. (Closes: reproducible-builds/diffoscope#245)
* Fix test_libmix_differences on openSUSE Tumbleweed.
  (Closes: reproducible-builds/diffoscope#244)
* Move test_rpm to use the assert_diff utility helper.

[ Hans-Christoph Steiner ]
* Add a diffoscope.tools.get_tools() method to support programmatically
  fetching Diffoscope's config.

[ Roland Clobus ]
* Become tolerant of malformed Debian .changes files.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
