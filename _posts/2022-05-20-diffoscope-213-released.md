---
layout: post
title: diffoscope 213 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `213`. This version includes the following changes:

```
* Don't mask differences in .zip/.jar central directory extra fields.
* Don't show a binary comparison of .zip/.jar files if we have at least
  one observed nested difference.
* Use assert_diff in test_zip over get_data and separate assert.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
