---
layout: post
title: diffoscope 91 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `91`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* d/clean: Remove .cache/.
* diff: Wrap long regular expression for improved readability.
* comparators.json: Fix UnicodeDecodeError with a non-UTF8 locale.
* d/copyright: Update for the new year.
* d/control: Bump Standards-Version to 4.1.3, no changes needed.
* tests/android:
  + Skip tests involving abootimg on BE archs (see #725729).

[ Daniel Shahaf ]
* diffoscope.diff: Document feeders.  Closes: #863880
* Optimize the common case of feeders.  Closes: #863879

[ Juliana Oliveira ]
* {tempfiles, utils.archive}: catch possible FileNotFoundError.
* comparators.java: add support for procyon-decompiler.  Closes: #849386
* {command, feeders, diff}: replaces subprocess.Popen() by .run().
  This is done in preparation for work on multiprocessing (#842837).
  + {command, zip, feeders}: replaces .wait by the new .returncode property.
  + comparators.utils.command: replaces .stderr_content by .stderr.
* tests:
  + test_progress: fix test case for some broken versions of
    python-progressbar.  Closes: #877726
  + utils.tools: add support for modules on skip_unless* annotations.

[ Chris Lamb ]
* comparators:
  + utils/compare:
    - Show extended filesystem metadata even when directly comparing two
      files, not just when we specify two directories.  Closes: #888402
  + macho:
    - If the If the LLVM disassembler does not work, try the
      internal one.  Closes: #886736
    - Always strip the filename, not just when by itself.
  + json:
    - Do some cheap fuzzy parsing to detect JSON files not named .json.
    - Also match unicode JSON files.
    - Optionally compare JSONs with the jsondiff module.  Closes: #888112
  + directory:
    - Report differences in extended file attributes when comparing files.
      Closes: #888401
  + xsb:
    - Add support for comparing XMLBeans binary schemas.
  + barkeley_db:
    - Add support for comparing Berkeley DB files.  Closes: #890528
* Misc code cleaup.
* tests:
  + comparators.test_elf: Return '0' if we can't parse the readelf
    version number.  Closes: #886963
* debian:
  + Explicitly build-depend and recommend e2fsprogs.  Closes: #887180

[ Ximin Luo ]
* Partially revert the changes done for #888402 to maintain the current.
  behaviour of --exclude-directory-metadata.
* Refactor how the configuration is loaded:
  + Move the defaults into a Config.reset() method.
  + reset() the configuration at the end of main(), to help the testsuite.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
