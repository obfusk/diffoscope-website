---
layout: post
title: diffoscope 230 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `230`. This version includes the following changes:

```
[ Chris Lamb ]
* Fix compatibility with file(1) version 5.43; thanks, Christoph Biedl.

[ Jelle van der Waa ]
* Support Berkeley DB version 6.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
