---
layout: post
title: diffoscope 30 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `30`. This version includes the following changes:

```
* Remove empty line at the beginning of HTML reports.
* Stop failing to run the tests when uid 1000 doesn't exist.
* Make .changes file matching more accurate.
* Move the sanity check for version number from clean to build rule.
* Remove leftover debug in squashfs comparator.
* Stop decompressing squashfs directories as we compare content already.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
