---
layout: post
title: diffoscope 175 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `175`. This version includes the following changes:

```
* Use the actual filesystem path name (instead of diffoscope's concept of the
  source name) to correct APK filename filtering when an APK file is in
  another container -- we need to filter the auto-generated "1.apk" instead
  of "original-filename.apk". (Closes: reproducible-builds/diffoscope#255)
* Don't call os.path.basename twice.
* Correct grammar in a fsimage.py debug message.
* Add a comment about stripping filenames.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
