---
layout: post
title: diffoscope 222 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `222`. This version includes the following changes:

```
[ Mattia Rizzolo ]
* Use pep517 and pip to load the requirements. (Closes: #1020091)
* Remove old Breaks/Replaces in debian/control that have been obsoleted since
  bullseye
```

You find out more by [visiting the project homepage](https://diffoscope.org).
