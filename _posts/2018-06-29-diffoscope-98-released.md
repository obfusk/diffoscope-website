---
layout: post
title: diffoscope 98 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `98`. This version includes the following changes:

```
* Fix compatibility with Python 3.7. (Closes: #902650)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
