---
layout: post
title: diffoscope 121 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `121`. This version includes the following changes:

```
* Don't fallback to a (useless) raw hexdump when readelf(1) reports an minor
  issue in a section in an ELF binary. For example, when the "frames" section
  is of the "NOBITS" type, its contents are apparently "unreliable" and thus
  readelf(1) exits with a returncode of 1. (Closes: #849407, #931962)
* Add support to Difference.from_command_exc and friends to optionally ignore
  specified returncodes from the called program and treat them as "no"
  difference.
* Simplify the parsing of the optional "command_args" argument to the
  from_command and from_command_exc methods in the Difference class.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
