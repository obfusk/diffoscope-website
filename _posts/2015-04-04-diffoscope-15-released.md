---
layout: post
title: diffoscope 15 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `15`. This version includes the following changes:

```
* Terminate xxd if its output was too long.
* Handle broken zip files by falling back on binary comparison.
* Fix an over-matching jar/war filename regexp.
* Fix .hi comparator.
* Fix some file descriptor leaks.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
