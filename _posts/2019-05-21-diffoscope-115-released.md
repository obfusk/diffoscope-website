---
layout: post
title: diffoscope 115 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `115`. This version includes the following changes:

```
[ Chris Lamb ]
* Fix execution of symbolic links that point to the "bin/diffoscope" entry
  point by fully resolving the location.
* Add a Dockerfile.
* Update contributing messages with instructions regarding the Docker image.

[ Mattia Rizzolo ]
* tests:
  + Allow specifying which tools are known missing using
  DIFFOSCOPE_TESTS_MISSING_TOOLS, to override a _FAIL_ON_MISSING_TOOLS.
  + With DIFFOSCOPE_TESTS_FAIL_ON_MISSING_TOOLS=1, actually fail only tests
    that are missing the required tools.
* debian:
  + Add black to the test dependencies.
  + Enforce the "fail on missing tools" only when testing with all the
    recommended packages.
  + Ack some missing tools during autopkgtest.
  + Reinstate oggvideotools and procyon-decompiler test dependencies,
    now that are not buggy anymore.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
