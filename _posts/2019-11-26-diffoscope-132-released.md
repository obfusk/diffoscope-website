---
layout: post
title: diffoscope 132 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `132`. This version includes the following changes:

```
* Allow all possible .zip variations to return with non-zero exit codes, not
  just known types we can explicitly identify (eg. Java .jmod and .jar
  files). (Closes: reproducible-builds/diffoscope#78)
* Also permit UTF-8 encoded .dsc and .changes files.
  (Re: reproducible-builds/diffoscope#78)
* Rework a long string of "or" statements into a loop with a "break".
```

You find out more by [visiting the project homepage](https://diffoscope.org).
