---
layout: post
title: diffoscope 221 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `221`. This version includes the following changes:

```
* Don't crash if we can open a PDF file with PyPDF but cannot parse the
  annotations within. (Closes: reproducible-builds/diffoscope#311)
* Depend on the dedicated xxd package, not vim-common.
* Update external_tools.py to reflect xxd/vim-common change.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
