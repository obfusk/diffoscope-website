---
layout: post
title: diffoscope 29 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `29`. This version includes the following changes:

```
[ Reiner Herrmann ]
* Prevent empty line at the end of squashfs listing.

[ Jérémy Bobbio ]
* Rename to diffoscope. debbindiff has grown way beyond a being just a tool
  to compare Debian packages. Let's rename it to better reflect this state of
  things.
* Add a favicon to HTML reports.
* Always use pybuild in debian/rules.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
