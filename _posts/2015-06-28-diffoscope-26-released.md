---
layout: post
title: diffoscope 26 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `26`. This version includes the following changes:

```
* Declare the package autopkgtestable.
* Fix comparator for unknown files.
* Add tests for unknown files comparator.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
