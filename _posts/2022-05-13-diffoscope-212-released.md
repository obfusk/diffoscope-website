---
layout: post
title: diffoscope 212 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `212`. This version includes the following changes:

```
* Add support for extracting vmlinuz/vmlinux Linux kernel images.
  (Closes: reproducible-builds/diffoscope#304)
* Some Python .pyc files report as "data", so support ".pyc" as a
  fallback extension.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
