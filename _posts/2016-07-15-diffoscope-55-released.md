---
layout: post
title: diffoscope 55 released
author: Reiner Herrmann <reiner@reiner-h.de>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `55`. This version includes the following changes:

```
[ anthraxx ]
* Fix sqlite3 magic recognition when using file >= 5.27. (Closes: #830434)

[ Satyam Zode ]
* Add argument completion feature to diffoscope. (Closes: #826711)

[ Chris Lamb ]
* Improve message when data differs. (Closes: #827981)

[ Reiner Herrmann ]
* Clarify the input arguments. (Closes: #826894)

[ Holger Levsen ]
* Add link to homepage in HTML output.

[ Rainer Müller ]
* python-magic >=0.4.12 does not require decode.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
