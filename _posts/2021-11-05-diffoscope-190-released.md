---
layout: post
title: diffoscope 190 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `190`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't raise a traceback if we cannot de-marshal Python bytecode to support
  Python 3.7 loading newer .pyc files.
  (Closes: reproducible-builds/diffoscope#284)
* Fix Python tests under Python 3.7 with file 5.39+.

[ Vagrant Cascadian ]
* Skip Python bytecode testing when "file" is older than 5.39.

[ Roland Clobus ]
* Detect whether the GNU_BUILD_ID field has been modified.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
