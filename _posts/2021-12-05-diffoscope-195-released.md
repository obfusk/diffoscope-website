---
layout: post
title: diffoscope 195 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `195`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't use the runtime platform's native endianness when unpacking .pyc
  files to fix test failures on big-endian machines.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
