---
layout: post
title: diffoscope 209 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `209`. This version includes the following changes:

```
* Update R test fixture for R 4.2.x series. (Closes: #1008446)
* Update minimum version of Black to prevent test failure on Ubuntu jammy.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
