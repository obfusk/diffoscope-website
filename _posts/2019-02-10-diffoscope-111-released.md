---
layout: post
title: diffoscope 111 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `111`. This version includes the following changes:

```
* Fix a regression introduced via #920701 where we stopped using the -dbgsym
  packages when comparing .buildinfo or .changes files. (Closes:
  reproducible-builds/diffoscope#46)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
