---
layout: post
title: diffoscope 21 released
author: Reiner Herrmann <reiner@reiner-h.de>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `21`. This version includes the following changes:

```
* Non-maintainer upload.
* Add support for Java .class files.
* Add support for .ipk package files.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
