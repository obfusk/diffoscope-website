---
layout: post
title: diffoscope 227 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `227`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't attempt to attach text-only differences notice if there are no
  differences to begin with. (Closes: #1024171, #1024349)
* Don't run Python decompiling tests on Python bytecode that both file(1)
  cannot yet detect and Python 3.11 cannot demarshall. (Closes: #1024335)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
