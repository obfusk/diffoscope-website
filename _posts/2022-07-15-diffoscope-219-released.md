---
layout: post
title: diffoscope 219 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `219`. This version includes the following changes:

```
* Don't traceback if we encounter an invalid Unicode character in Haskell
  versioning headers. (Closes: reproducible-builds/diffoscope#307)
* Update various copyright years.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
