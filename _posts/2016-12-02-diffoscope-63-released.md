---
layout: post
title: diffoscope 63 released
author: Ximin Luo <infinity0@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `63`. This version includes the following changes:

```
* Add myself to Uploaders.
* Greatly improve speed for large archives by fixing O(n^2) complexity for
  archive member lookup.
  - There is still O(n^2) complexity for archive member extraction, but this
    is less noticeable for various reasons and would require more complexity
    to fix, so for now is left as a task for the future.
* Text output: add coloured diff support via colordiff(1).
* Html-dir output: add +/- buttons to fold sub-diffs (i.e. toggle their
  visibility) as well as the whole diff itself. As with similar features in
  other programs, the effect affects all descendants if you shift-click.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
