---
layout: post
title: diffoscope 153 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `153`. This version includes the following changes:

```
[ Chris Lamb ]

* Drop some legacy argument styles; --exclude-directory-metadata and
  --no-exclude-directory-metadata have been replaced with
  --exclude-directory-metadata={yes,no}.

* Code improvements:

  - Make it easier to navigate the main.py entry point.
  - Use a relative import for get_temporary_directory in diffoscope.diff.
  - Rename bail_if_non_existing to exit_if_paths_do_not_exist.
  - Rewrite exit_if_paths_do_not_exist to not check files multiple times.

* Documentation improvements:

  - CONTRIBUTING.md:

    - Add a quick note about adding/suggesting new options.
    - Update and expand the release process documentation.
    - Add a reminder to regenerate debian/tests/control.

  - README.rst:

    - Correct URL to build job on Jenkins.
    - Clarify and correct contributing info to point to salsa.debian.org.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
