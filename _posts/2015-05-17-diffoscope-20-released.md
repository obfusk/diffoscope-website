---
layout: post
title: diffoscope 20 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `20`. This version includes the following changes:

```
[ Reiner Herrmann ]
* info files can contain numbers in the extension

[ Jérémy Bobbio ]
* Fix diff parser when skipping until the end of a file without a newline.
* Use same encoding as stdin when output is not a TTY. (Closes: #785288)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
