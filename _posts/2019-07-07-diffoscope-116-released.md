---
layout: post
title: diffoscope 116 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `116`. This version includes the following changes:

```
[ Chris Lamb ]
* Upload to unstable after the release of Debian "buster".
* README.rst & manual page generation:
  - Strip out manpage-only parts of the README rather than using "only"
    reStructuredText directives in order to support the demands of the latest
    PyPI website.
  - Use "real" reStructuredText comments instead of using the "raw" directive.
* Dockerfile:
  - Build the Docker image from the current Git checkout, not the Debian
    archive. (reproducible-builds/diffoscope#56)
  - Use the ENTRYPOINT directive with the JSON syntax instead so we pass all
    arguments to the underlying diffoscope executable.
* Document that run_diffoscope should not be considered a stable API.

[ Mattia Rizzolo ]
* Rename a test function to prevent shadowing a previous one with the
  same name.
* Add ffmpeg to the list of Debian build-dependencies for the testsuite.

[ Vagrant Cascadian ]
* Add support for known external tools in GNU Guix.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
