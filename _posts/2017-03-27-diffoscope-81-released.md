---
layout: post
title: diffoscope 81 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `81`. This version includes the following changes:

```
[ Chris Lamb ]
* Correct meaningless "1234-content" metadata when introspecting files
  within archives. This was a regression since #854723 due to the use of
  auto-incrementing on-disk filenames. (Closes: #858223)
* Refactor get_compressed_content_name.

[ Ximin Luo ]
* Improve ISO9660/DOS/MBR check.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
