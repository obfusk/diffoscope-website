---
layout: post
title: diffoscope 142 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `142`. This version includes the following changes:

```
[ Chris Lamb ]
* Render multiline difference comments in a way to show indentation.
  (Closes: reproducible-builds/diffoscope#101)
* Correct parsing of "./setup.py test --pytest-args ...".
* Capitalise "Ordering differences only" in text comparison comments.
* Don't include the JSON similarity percentage if it is 0.0%.

[ Ben Hutchings ]
* Document how --exclude arguments are matched against filenames.

[ Vagrant Cascadian ]
* Add external tool reference for h5dump on Guix.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
