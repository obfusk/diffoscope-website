---
layout: post
title: diffoscope 192 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `192`. This version includes the following changes:

```
* Update .epub test methodology after improving XML file parsing.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
