---
layout: post
title: diffoscope 75 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `75`. This version includes the following changes:

```
[ Chris Lamb ]
* Fix ImportError in Debian comparator tests. This was caused by not
  handling the case where ``importlib.find_spec`` was testing for a submodule
  (ie. ``debian.Deb822``) where it will attempt to import the ``debian``
  module and raise an exception if it does not exist. Thanks to Iain Lane for
  initial patches. (Closes: #854670)

[ Ximin Luo ]
* Remove pointless use of a thread
```

You find out more by [visiting the project homepage](https://diffoscope.org).
