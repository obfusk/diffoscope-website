---
layout: post
title: diffoscope 96 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `96`. This version includes the following changes:

```
[ Chris Lamb ]
* Drop dependency on pdftk as it relies on GCJ, relying on the pdftotext
  fallback. (Closes: #893702)
* Change the "No file format specific differences found inside, yet data
  differs" message to be clearer that diffoscope "knows" about this file
  format yet could not be helpful in this case.
* Don't append a rather useless "(data)" suffix from file(1).
* Comply with a number of PEP8 recommendations:
  - E226 - Add missing whitespaces around operators.
  - E241 - Fix extraneous whitespaces around keywords.
  - E251 - Remove whitespace around parameter '=' signs.
  - E302 - Add missing 2 blank lines.
  - E501 - Try to make lines fit to length.
  - E502 - Remove extraneous escape of newline.
  - E731 - Don't assign lambda expressions.
  - E121, E122, E126, E128 - Fix badly indented lines.

[ Xavier Briand ]
* Add merge request details to contributing documentation.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
