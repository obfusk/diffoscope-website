---
layout: post
title: diffoscope 94 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `94`. This version includes the following changes:

```
[ Chris Lamb ]
* Update tests to prevent FTBFS under file 5.33. (Closes: #897099)
* Remove all __pycache__ directories to avoid source-contains-empty-directory
  Lintian warning.
* Remove unused test1.txt test data file.
* Bump Standards-Version to 4.1.4.

[ anthraxx ]
* Add gnumeric to the list of ArchLinux tools.

[ Paul Wise ]
* Sort the Debian dependencies.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
