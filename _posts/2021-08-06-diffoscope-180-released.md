---
layout: post
title: diffoscope 180 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `180`. This version includes the following changes:

```
* Don't include specific ".debug"-like lines in the output, as it invariably
  a duplicate of the debug ID that exists in a better form in the readelf(1)
  differences for this file.
* Also ignore include short "GCC" lines that differs on a single prefix byte
  too. These are distracting, not very useful and are simply the strings(1)
  command's idea of the build ID, which, again, is displayed nearby in the
  file's diff.
* Update the invocation arguments and tests for the latest version of
  odt2txt.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
