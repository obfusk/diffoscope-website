---
layout: post
title: diffoscope 155 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `155`. This version includes the following changes:

```
[ Chris Lamb ]
* Bump Python requirement from 3.6 to 3.7 - most distributions are either
  shipping3.5 or 3.7, so supporting 3.6 is not somewhat unnecessary and also
  more difficult to test locally.
* Improvements to setup.py:
  - Apply the Black source code reformatter.
  - Add some URLs for the site of PyPI.org.
  - Update "author" and author email.
* Explicitly support Python 3.8.

[ Frazer Clews ]
* Move away from the deprecated logger.warn method logger.warning.

[ Mattia Rizzolo ]
* Document ("classify") on PyPI that this project works with Python 3.8.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
