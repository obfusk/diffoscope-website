---
layout: post
title: diffoscope 88 released
author: Mattia Rizzolo <mattia@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `88`. This version includes the following changes:

```
[ Holger Levsen ]
* Bump standards version to 4.1.1, no changes needed.

[ Mattia Rizzolo ]
* tests/comparators:
  + dtb: compatibility with version 1.4.5.  Closes: #880279

[ Chris Lamb ]
* comparators:
  + binwalk: improve names in output of "internal" members.  Closes: #877525
  + Omit misleading "any of" prefix when only complaining about one module
    in ImportError messages.
* Don't crash on malformed md5sums files.  Closes: #877473
* tests/comparators:
  + ps: ps2ascii > 9.21 now varies on timezone, so skip this test for now.
  + dtby: only parse the version number, not any "-dirty" suffix.
* debian/watch: use HTTPS URI.

[ Ximin Luo ]
* comparators:
  + utils/file: diff container metadata centrally.  Closes: #797759
    This fixes a last remaining bug in fuzzy-matching across containers.
  + Fix all the affected comparators after the above change.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
