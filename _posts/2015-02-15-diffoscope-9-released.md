---
layout: post
title: diffoscope 9 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `9`. This version includes the following changes:

```
[ Chris Lamb ]
* Add rpm-common to Build-Depends so that we can “import rpm" during build.
* Code improvements improvements.

[ Jérémy Bobbio ]
* Add plain text output for differences. Thanks Helmut Grohne for the
  original patch. (Closes: #778423)
* Exit with 2 in case of errors. (Closes: #774983)
* Properly output diffs when one of the file is empty.
* Add support for comparing directories.
* Update debian/copyright.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
