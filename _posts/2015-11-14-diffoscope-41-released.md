---
layout: post
title: diffoscope 41 released
author: Jérémy Bobbio <lunar@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `41`. This version includes the following changes:

```
* Minor rewordings in README.
* Fix link in README. Thanks anthraxx.
* Also skip tests when very common tools are unavailable.
* Add git-buildpackage configuration in the hope it will help to get proper
  source tarball.
* Allow commands to be called with specified environment variables.
* Force ppudump to output time in UTC by setting TZDIR in the environment.
```

You find out more by [visiting the project homepage](https://diffoscope.org).
