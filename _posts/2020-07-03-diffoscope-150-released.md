---
layout: post
title: diffoscope 150 released
author: Chris Lamb <lamby@debian.org>
---

The diffoscope maintainers are pleased to announce the release of diffoscope
version `150`. This version includes the following changes:

```
[ Chris Lamb ]
* Don't crash when listing entries in archives if they don't have a listed
  size (such as hardlinks in .ISO files).
  (Closes: reproducible-builds/diffoscope#188)
* Dump PE32+ executables (including EFI applications) using objdump.
  (Closes: reproducible-builds/diffoscope#181)
* Tidy detection of JSON files due to missing call to File.recognizes that
  checks against the output of file(1) which was also causing us to attempt
  to parse almost every file using json.loads. (Whoops.)
* Drop accidentally-duplicated copy of the new --diff-mask tests.
* Logging improvements:
  - Split out formatting of class names into a common method.
  - Clarify that we are generating presenter formats in the opening logs.

[ Jean-Romain Garnier ]
* Remove objdjump(1) offsets before instructions to reduce diff noise.
  (Closes: reproducible-builds/diffoscope!57)
```

You find out more by [visiting the project homepage](https://diffoscope.org).
